---
Type: Reading Notes
Book Title: Machine Learning In Action
Subtitle: Chapter 5 - Logistic Regression
---

# Chapter 5 - Logistic Regression

## 5.1 Classification with logistic regression and the sigmoid function: a tractable step function

> Logistic regression
>
> Pros: Computationally inexpensive, easy to implement, knowledge representation easy to interpret
>
> Cons: Prone to underfitting, may have low accuracy
>
> Works with: Numeric values, nominal values

The reason why we don't use the **Heaviside step function** is that at the point where it steps from 0 to 1, it does so instantly. The instantaneous step is sometimes difficult to deal with. We would use the **sigmoid function** instead.
